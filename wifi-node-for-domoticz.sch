EESchema Schematic File Version 4
LIBS:wifi-node-for-domoticz-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP-12E U1
U 1 1 5C058091
P 5000 3750
F 0 "U1" H 5000 4728 50  0000 C CNN
F 1 "ESP-12E" H 5000 4637 50  0000 C CNN
F 2 "RF_Module:ESP-12E" H 5000 3750 50  0001 C CNN
F 3 "http://wiki.ai-thinker.com/_media/esp8266/esp8266_series_modules_user_manual_v1.1.pdf" H 4650 3850 50  0001 C CNN
	1    5000 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5C058522
P 5000 4550
F 0 "#PWR02" H 5000 4300 50  0001 C CNN
F 1 "GND" H 5005 4377 50  0000 C CNN
F 2 "" H 5000 4550 50  0001 C CNN
F 3 "" H 5000 4550 50  0001 C CNN
	1    5000 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4450 5000 4550
$Comp
L power:+3.3V #PWR01
U 1 1 5C059195
P 5000 2650
F 0 "#PWR01" H 5000 2500 50  0001 C CNN
F 1 "+3.3V" H 5015 2823 50  0000 C CNN
F 2 "" H 5000 2650 50  0001 C CNN
F 3 "" H 5000 2650 50  0001 C CNN
	1    5000 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2650 5000 2950
$Comp
L Power_Supplies:HLK-PM05 U4
U 1 1 5C0597C8
P 8850 1400
F 0 "U4" H 8850 1642 40  0000 C CNN
F 1 "HLK-PM05" H 8850 1566 40  0000 C CNN
F 2 "Power_Supply:HLK-PM05" H 8850 1750 60  0000 C CNN
F 3 "" H 7950 2750 60  0000 C CNN
	1    8850 1400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5C05997B
P 8300 1350
F 0 "J1" H 8220 1025 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 8220 1116 50  0000 C CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 8300 1350 50  0001 C CNN
F 3 "~" H 8300 1350 50  0001 C CNN
	1    8300 1350
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR011
U 1 1 5C059A21
P 9350 1350
F 0 "#PWR011" H 9350 1200 50  0001 C CNN
F 1 "+5V" H 9365 1523 50  0000 C CNN
F 2 "" H 9350 1350 50  0001 C CNN
F 3 "" H 9350 1350 50  0001 C CNN
	1    9350 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5C059A50
P 9350 1450
F 0 "#PWR012" H 9350 1200 50  0001 C CNN
F 1 "GND" H 9355 1277 50  0000 C CNN
F 2 "" H 9350 1450 50  0001 C CNN
F 3 "" H 9350 1450 50  0001 C CNN
	1    9350 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 1350 9350 1350
Wire Wire Line
	9150 1450 9350 1450
$Comp
L power:+5V #PWR07
U 1 1 5C059D4A
P 9750 1300
F 0 "#PWR07" H 9750 1150 50  0001 C CNN
F 1 "+5V" H 9765 1473 50  0000 C CNN
F 2 "" H 9750 1300 50  0001 C CNN
F 3 "" H 9750 1300 50  0001 C CNN
	1    9750 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5C059D61
P 10200 1700
F 0 "#PWR08" H 10200 1450 50  0001 C CNN
F 1 "GND" H 10205 1527 50  0000 C CNN
F 2 "" H 10200 1700 50  0001 C CNN
F 3 "" H 10200 1700 50  0001 C CNN
	1    10200 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR09
U 1 1 5C059D8B
P 10650 1300
F 0 "#PWR09" H 10650 1150 50  0001 C CNN
F 1 "+3.3V" H 10665 1473 50  0000 C CNN
F 2 "" H 10650 1300 50  0001 C CNN
F 3 "" H 10650 1300 50  0001 C CNN
	1    10650 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 1300 10600 1300
Wire Wire Line
	9750 1300 9850 1300
Wire Wire Line
	10200 1600 10200 1700
$Comp
L Sensor_Temperature:DS18B20 U2
U 1 1 5C059FB7
P 5500 1450
F 0 "U2" H 5270 1496 50  0000 R CNN
F 1 "DS18B20" H 5270 1405 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4500 1200 50  0001 C CNN
F 3 "http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf" H 5350 1700 50  0001 C CNN
	1    5500 1450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C05A1DB
P 5500 1800
F 0 "#PWR04" H 5500 1550 50  0001 C CNN
F 1 "GND" H 5505 1627 50  0000 C CNN
F 2 "" H 5500 1800 50  0001 C CNN
F 3 "" H 5500 1800 50  0001 C CNN
	1    5500 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 1100 5500 1150
Wire Wire Line
	5500 1750 5500 1800
$Comp
L Connector:Screw_Terminal_01x03 J2
U 1 1 5C05BAEA
P 9900 4600
F 0 "J2" H 9980 4642 50  0000 L CNN
F 1 "Screw_Terminal_01x03" H 9980 4551 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-3_P5.08mm" H 9900 4600 50  0001 C CNN
F 3 "~" H 9900 4600 50  0001 C CNN
	1    9900 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 4600 8550 4600
Wire Wire Line
	9700 4650 9700 4700
$Comp
L Device:R R4
U 1 1 5C05C82A
P 7150 3950
F 0 "R4" V 6943 3950 50  0000 C CNN
F 1 "1kR" V 7034 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 7080 3950 50  0001 C CNN
F 3 "~" H 7150 3950 50  0001 C CNN
	1    7150 3950
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5C05C9A9
P 7400 4200
F 0 "R5" H 7330 4154 50  0000 R CNN
F 1 "10kR" H 7330 4245 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 7330 4200 50  0001 C CNN
F 3 "~" H 7400 4200 50  0001 C CNN
	1    7400 4200
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5C05D553
P 7400 4450
F 0 "#PWR05" H 7400 4200 50  0001 C CNN
F 1 "GND" H 7405 4277 50  0000 C CNN
F 2 "" H 7400 4450 50  0001 C CNN
F 3 "" H 7400 4450 50  0001 C CNN
	1    7400 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5C05DB9D
P 7850 4200
F 0 "#PWR06" H 7850 3950 50  0001 C CNN
F 1 "GND" H 7855 4027 50  0000 C CNN
F 2 "" H 7850 4200 50  0001 C CNN
F 3 "" H 7850 4200 50  0001 C CNN
	1    7850 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 4150 7850 4200
$Comp
L power:GND #PWR0102
U 1 1 5C05FE1F
P 6250 1600
F 0 "#PWR0102" H 6250 1350 50  0001 C CNN
F 1 "GND" H 6255 1427 50  0000 C CNN
F 2 "" H 6250 1600 50  0001 C CNN
F 3 "" H 6250 1600 50  0001 C CNN
	1    6250 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 1550 6250 1600
$Comp
L Device:C C1
U 1 1 5C060969
P 9850 1450
F 0 "C1" H 9965 1496 50  0000 L CNN
F 1 "C" H 9965 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 9888 1300 50  0001 C CNN
F 3 "~" H 9850 1450 50  0001 C CNN
	1    9850 1450
	1    0    0    -1  
$EndComp
Connection ~ 9850 1300
Wire Wire Line
	9850 1300 9900 1300
$Comp
L Device:C C2
U 1 1 5C0609DF
P 10600 1450
F 0 "C2" H 10715 1496 50  0000 L CNN
F 1 "C" H 10715 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 10638 1300 50  0001 C CNN
F 3 "~" H 10600 1450 50  0001 C CNN
	1    10600 1450
	1    0    0    -1  
$EndComp
Connection ~ 10600 1300
Wire Wire Line
	10600 1300 10650 1300
Wire Wire Line
	9850 1600 10200 1600
Connection ~ 10200 1600
Wire Wire Line
	10200 1600 10600 1600
Wire Wire Line
	9250 4250 9250 4650
Wire Wire Line
	9250 4650 9700 4650
Wire Wire Line
	8500 1350 8550 1350
Wire Wire Line
	8500 1450 8550 1450
Wire Wire Line
	7550 3950 7400 3950
Wire Wire Line
	7400 4050 7400 3950
Connection ~ 7400 3950
Wire Wire Line
	7400 3950 7300 3950
Wire Wire Line
	7400 4350 7400 4450
$Comp
L power:+5V #PWR0103
U 1 1 5C0B3399
P 9350 3750
F 0 "#PWR0103" H 9350 3600 50  0001 C CNN
F 1 "+5V" H 9365 3923 50  0000 C CNN
F 2 "" H 9350 3750 50  0001 C CNN
F 3 "" H 9350 3750 50  0001 C CNN
	1    9350 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 3750 9200 3750
$Comp
L pspice:DIODE D1
U 1 1 5C0B41FC
P 8850 3250
F 0 "D1" H 8850 3515 50  0000 C CNN
F 1 "DIODE" H 8850 3424 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P10.16mm_Horizontal" H 8850 3250 50  0001 C CNN
F 3 "~" H 8850 3250 50  0001 C CNN
	1    8850 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 3750 9200 3250
Wire Wire Line
	9200 3250 9050 3250
Connection ~ 9200 3750
Wire Wire Line
	9200 3750 9350 3750
Wire Wire Line
	8650 3250 8500 3250
Wire Wire Line
	8500 3250 8500 3750
Connection ~ 8500 3750
Wire Wire Line
	8500 3750 8550 3750
Text GLabel 5750 3250 2    50   Input ~ 0
Tx
Text GLabel 5750 3450 2    50   Input ~ 0
Rx
Wire Wire Line
	5750 3450 5600 3450
Wire Wire Line
	5600 3250 5750 3250
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5C0CF41F
P 6450 1450
F 0 "J3" H 6423 1473 50  0000 R CNN
F 1 "Conn_01x03_Male" H 6423 1382 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6450 1450 50  0001 C CNN
F 3 "~" H 6450 1450 50  0001 C CNN
	1    6450 1450
	-1   0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_DSG Q1
U 1 1 5C0E8FAA
P 7750 3950
F 0 "Q1" H 7955 3996 50  0000 L CNN
F 1 "AO3400" H 7955 3905 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7950 4050 50  0001 C CNN
F 3 "~" H 7750 3950 50  0001 C CNN
	1    7750 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 3750 8050 3750
$Comp
L Regulator_Linear:AMS1117 U3
U 1 1 5C0EC117
P 10200 1300
F 0 "U3" H 10200 1542 50  0000 C CNN
F 1 "AMS1117" H 10200 1451 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 10200 1500 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 10300 1050 50  0001 C CNN
	1    10200 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 5C0FC010
P 8050 3500
F 0 "D2" V 8088 3383 50  0000 R CNN
F 1 "LED" V 7997 3383 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric" H 8050 3500 50  0001 C CNN
F 3 "~" H 8050 3500 50  0001 C CNN
	1    8050 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 5C0FC0F8
P 8050 3150
F 0 "R1" H 8120 3196 50  0000 L CNN
F 1 "R" H 8120 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 7980 3150 50  0001 C CNN
F 3 "~" H 8050 3150 50  0001 C CNN
	1    8050 3150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 5C0FC188
P 8050 2950
F 0 "#PWR0106" H 8050 2800 50  0001 C CNN
F 1 "+5V" H 8065 3123 50  0000 C CNN
F 2 "" H 8050 2950 50  0001 C CNN
F 3 "" H 8050 2950 50  0001 C CNN
	1    8050 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 2950 8050 3000
Wire Wire Line
	8050 3300 8050 3350
Wire Wire Line
	8050 3650 8050 3750
Connection ~ 8050 3750
Wire Wire Line
	8050 3750 8500 3750
Text GLabel 5750 4300 2    50   Input ~ 0
Relay
Text GLabel 6900 3950 0    50   Input ~ 0
Relay
Wire Wire Line
	6900 3950 7000 3950
Text GLabel 5750 3850 2    50   Input ~ 0
P7_GPIO13
Wire Wire Line
	5600 3850 5750 3850
$Comp
L Relay:SANYOU_SRD_Form_C K2
U 1 1 5C115EB0
P 8850 3950
F 0 "K2" V 8283 3950 50  0000 C CNN
F 1 "SANYOU_SRD_Form_C" V 8374 3950 50  0000 C CNN
F 2 "Relay_THT:Relay_SPDT_SANYOU_SRD_Series_Form_C" H 9300 3900 50  0001 L CNN
F 3 "http://www.sanyourelay.ca/public/products/pdf/SRD.pdf" H 8850 3950 50  0001 C CNN
	1    8850 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 4250 9150 4250
Wire Wire Line
	9150 4050 9700 4050
Wire Wire Line
	9700 4050 9700 4500
Wire Wire Line
	8550 4150 8550 4600
Text GLabel 4300 2900 0    50   Input ~ 0
P1_RST
Text GLabel 4250 3550 0    50   Input ~ 0
P2_ADC
Text GLabel 4250 3350 0    50   Input ~ 0
P3_EN
Text GLabel 5750 3950 2    50   Input ~ 0
P5_GPIO14
Text GLabel 5750 3750 2    50   Input ~ 0
P6_GPIO12
Text GLabel 1400 2750 2    50   Input ~ 0
P1_RST
Text GLabel 1400 2950 2    50   Input ~ 0
P3_EN
Text GLabel 1400 2850 2    50   Input ~ 0
P2_ADC
Text GLabel 1400 3050 2    50   Input ~ 0
Relay
Text GLabel 1400 3150 2    50   Input ~ 0
P5_GPIO14
Text GLabel 1400 3250 2    50   Input ~ 0
P6_GPIO12
$Comp
L power:+3.3V #PWR010
U 1 1 5C15E100
P 1400 3550
F 0 "#PWR010" H 1400 3400 50  0001 C CNN
F 1 "+3.3V" H 1415 3723 50  0000 C CNN
F 2 "" H 1400 3550 50  0001 C CNN
F 3 "" H 1400 3550 50  0001 C CNN
	1    1400 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 2750 1400 2750
Wire Wire Line
	1300 2850 1400 2850
Wire Wire Line
	1300 2950 1400 2950
Wire Wire Line
	1300 3050 1400 3050
Wire Wire Line
	1300 3150 1400 3150
Wire Wire Line
	1300 3250 1400 3250
Wire Wire Line
	1300 3350 1400 3350
Wire Wire Line
	5600 3750 5750 3750
Wire Wire Line
	5600 3950 5750 3950
Wire Wire Line
	4250 3550 4400 3550
$Comp
L power:+3.3V #PWR013
U 1 1 5C172CD5
P 4000 3250
F 0 "#PWR013" H 4000 3100 50  0001 C CNN
F 1 "+3.3V" H 4015 3423 50  0000 C CNN
F 2 "" H 4000 3250 50  0001 C CNN
F 3 "" H 4000 3250 50  0001 C CNN
	1    4000 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3150 4400 2900
Wire Wire Line
	4400 2900 4300 2900
$Comp
L Device:R R2
U 1 1 5C174ED9
P 4200 3250
F 0 "R2" V 3993 3250 50  0000 C CNN
F 1 "1kR" V 4084 3250 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 4130 3250 50  0001 C CNN
F 3 "~" H 4200 3250 50  0001 C CNN
	1    4200 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3250 4050 3250
Wire Wire Line
	4250 3350 4350 3350
Wire Wire Line
	4350 3250 4350 3350
Connection ~ 4350 3350
Wire Wire Line
	4350 3350 4400 3350
$Comp
L Device:R R3
U 1 1 5C178EF7
P 5900 4050
F 0 "R3" V 5693 4050 50  0000 C CNN
F 1 "1kR" V 5784 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5830 4050 50  0001 C CNN
F 3 "~" H 5900 4050 50  0001 C CNN
	1    5900 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	5600 4050 5700 4050
$Comp
L power:GND #PWR014
U 1 1 5C17ADEE
P 6350 4050
F 0 "#PWR014" H 6350 3800 50  0001 C CNN
F 1 "GND" H 6355 3877 50  0000 C CNN
F 2 "" H 6350 4050 50  0001 C CNN
F 3 "" H 6350 4050 50  0001 C CNN
	1    6350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4150 5650 4150
Wire Wire Line
	5650 4150 5650 4300
Wire Wire Line
	5650 4300 5750 4300
Text GLabel 5750 4150 2    50   Input ~ 0
P16_GPIO15
Wire Wire Line
	6050 4050 6350 4050
Wire Wire Line
	5750 4150 5700 4150
Wire Wire Line
	5700 4150 5700 4050
Connection ~ 5700 4050
Wire Wire Line
	5700 4050 5750 4050
Text GLabel 5750 3650 2    50   Input ~ 0
P20_GPIO5
Text GLabel 5750 3550 2    50   Input ~ 0
P19_GPIO4
Text GLabel 5750 3150 2    50   Input ~ 0
P18_GPIO0
Text GLabel 5750 3350 2    50   Input ~ 0
P17_GPIO2
Wire Wire Line
	5600 3150 5750 3150
Wire Wire Line
	5600 3350 5750 3350
Wire Wire Line
	5600 3550 5750 3550
Wire Wire Line
	5600 3650 5750 3650
Text GLabel 2600 2750 2    50   Input ~ 0
Tx
Text GLabel 2600 2850 2    50   Input ~ 0
Rx
Text GLabel 2600 2950 2    50   Input ~ 0
P20_GPIO5
Text GLabel 2600 3050 2    50   Input ~ 0
P19_GPIO4
Text GLabel 2600 3150 2    50   Input ~ 0
P18_GPIO0
Text GLabel 2600 3250 2    50   Input ~ 0
P17_GPIO2
Text GLabel 2600 3350 2    50   Input ~ 0
P16_GPIO15
$Comp
L power:GND #PWR0107
U 1 1 5C190B97
P 2600 3550
F 0 "#PWR0107" H 2600 3300 50  0001 C CNN
F 1 "GND" H 2605 3377 50  0000 C CNN
F 2 "" H 2600 3550 50  0001 C CNN
F 3 "" H 2600 3550 50  0001 C CNN
	1    2600 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3350 2600 3350
Wire Wire Line
	2500 3250 2600 3250
Wire Wire Line
	2500 3150 2600 3150
Wire Wire Line
	2500 3050 2600 3050
Wire Wire Line
	2500 2950 2600 2950
Wire Wire Line
	2500 2850 2600 2850
Wire Wire Line
	2500 2750 2600 2750
$Comp
L Connector:Conn_01x10_Male J5
U 1 1 5C1BC6DE
P 1100 3150
F 0 "J5" H 1206 3728 50  0000 C CNN
F 1 "Conn_01x10_Male" H 1206 3637 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 1100 3150 50  0001 C CNN
F 3 "~" H 1100 3150 50  0001 C CNN
	1    1100 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x10_Male J6
U 1 1 5C1BC917
P 2300 3150
F 0 "J6" H 2406 3728 50  0000 C CNN
F 1 "Conn_01x10_Male" H 2406 3637 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 2300 3150 50  0001 C CNN
F 3 "~" H 2300 3150 50  0001 C CNN
	1    2300 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3450 2500 3550
Wire Wire Line
	2500 3550 2500 3650
Connection ~ 2500 3550
Wire Wire Line
	1300 3450 1300 3550
Wire Wire Line
	1300 3550 1300 3650
Connection ~ 1300 3550
$Comp
L power:+3.3V #PWR0101
U 1 1 5C1CBE38
P 5500 1100
F 0 "#PWR0101" H 5500 950 50  0001 C CNN
F 1 "+3.3V" H 5515 1273 50  0000 C CNN
F 2 "" H 5500 1100 50  0001 C CNN
F 3 "" H 5500 1100 50  0001 C CNN
	1    5500 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0104
U 1 1 5C1CBE6B
P 6250 1050
F 0 "#PWR0104" H 6250 900 50  0001 C CNN
F 1 "+3.3V" H 6265 1223 50  0000 C CNN
F 2 "" H 6250 1050 50  0001 C CNN
F 3 "" H 6250 1050 50  0001 C CNN
	1    6250 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5C1CE9B4
P 6050 1300
F 0 "R6" H 6120 1346 50  0000 L CNN
F 1 "4,7kR" H 6120 1255 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 5980 1300 50  0001 C CNN
F 3 "~" H 6050 1300 50  0001 C CNN
	1    6050 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1450 5850 1450
Wire Wire Line
	5800 750  5850 750 
Wire Wire Line
	5850 750  5850 1450
Connection ~ 5850 1450
Wire Wire Line
	5850 1450 6050 1450
Connection ~ 6050 1450
Wire Wire Line
	6050 1450 6250 1450
Wire Wire Line
	6050 1150 6050 1100
Wire Wire Line
	6050 1100 6250 1100
Wire Wire Line
	6250 1100 6250 1050
Wire Wire Line
	6250 1100 6250 1350
Connection ~ 6250 1100
Text GLabel 1400 3350 2    50   Input ~ 0
P7_GPIO13
Wire Wire Line
	1300 3550 1400 3550
Wire Wire Line
	2500 3550 2600 3550
Text GLabel 5800 750  0    50   Input ~ 0
P20_GPIO5
$Comp
L Device:R R7
U 1 1 5C183773
P 4400 2700
F 0 "R7" V 4193 2700 50  0000 C CNN
F 1 "10kR" V 4284 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 4330 2700 50  0001 C CNN
F 3 "~" H 4400 2700 50  0001 C CNN
	1    4400 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	4400 2850 4400 2900
Connection ~ 4400 2900
$Comp
L power:+3.3V #PWR03
U 1 1 5C186A72
P 4400 2500
F 0 "#PWR03" H 4400 2350 50  0001 C CNN
F 1 "+3.3V" H 4415 2673 50  0000 C CNN
F 2 "" H 4400 2500 50  0001 C CNN
F 3 "" H 4400 2500 50  0001 C CNN
	1    4400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2500 4400 2550
$EndSCHEMATC
